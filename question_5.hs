import Data.List
import Debug.Trace

valid :: [[Bool]] -> Bool
valid xs = validaux xs (length xs) && (length xs >= 3)
  where
    validaux [] y = True
    validaux (x:xs) y = (length x) == y && validaux xs y 


showGrid :: [[Bool]] -> String
showGrid [] = ""
showGrid (x:xs) = showLine x ++ showGrid xs
  where
    showLine [] = "\n"
    showLine (x:xs)  | x         = "0" ++ showLine xs
                     | otherwise = "." ++ showLine xs

printGrid :: [[Bool]] -> IO ()
printGrid x = putStrLn (showGrid x)


readGrid :: String -> [[Bool]]
readGrid x = readGridAux (lines x)
  where
    readGridAux [] = []
    readGridAux (x:xs) = [readLine x] ++ readGridAux xs
      where
        readLine [] = []
        readLine (x:xs) | x == '0' = True : readLine xs
                        | x == '.' = False : readLine xs


neighbours :: [Bool] -> Int
neighbours [] = 0
neighbours (x:xs) | x == True = 1 + neighbours xs
                  | otherwise = neighbours xs

newCell :: Int -> Bool -> Bool
newCell x y | y == True  && (x < 2 || x > 3) = False
            | y == True && (x == 2 || x == 3) = True
            | y == False && x == 3  = True
            | otherwise             = False

step :: [[Bool]] -> [[Bool]]
step (x:xs) = if (valid (x:xs)) 
                then processGrid (x:xs)
              else 
                error "Invalid grid"

processGrid :: [[Bool]] -> [[Bool]]
processGrid [] = []
processGrid (x:xs) = [processLine x (x:xs)] ++ processGrid xs


(!!!)                :: [[Bool]] -> Maybe Int -> [Bool]
xs     !!! Nothing        = []
[]     !!! _              = []
(x:_)  !!! Just 0         = x
(_:xs) !!! Just n         = xs !!! Just (n-1)

maybeSub :: Maybe Int -> Int -> Maybe Int
maybeSub Nothing x = Just (-1)
maybeSub (Just y) x =  Just (y - x)

maybeAdd :: Maybe Int -> Int -> Maybe Int
maybeAdd Nothing x = Just (-1)
maybeAdd (Just y) x = Just (y + x)

processLine :: [Bool] -> [[Bool]] -> [Bool]
processLine [] grid = []
processLine (x:xs) grid = newCell (neighbours neighboursList)  x : processLine xs grid
  where
    curLine = (x:xs)
    neighboursList = gs ++ hs ++ js
      where
        hs = 
             filter (\z -> elemIndex z curLine == (elemIndex x curLine `maybeSub` 1) || elemIndex z curLine == (elemIndex x curLine `maybeAdd` 1)) xs
        gs = 
             filter (\z -> elemIndex z curLine == (elemIndex x curLine `maybeSub` 1) || elemIndex z curLine == elemIndex x curLine || elemIndex z curLine == (elemIndex x curLine `maybeAdd` 1)) (grid !!! (elemIndex curLine grid `maybeSub` 1))
        js = 
             filter (\z -> elemIndex z curLine == (elemIndex x curLine `maybeSub` 1) || elemIndex z curLine == elemIndex x curLine || elemIndex z curLine == (elemIndex x curLine `maybeAdd` 1)) (grid !!! (elemIndex curLine grid `maybeAdd` 1))





 