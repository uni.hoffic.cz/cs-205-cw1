divides :: Integer -> Integer -> Bool
divides x y = y `mod` x == 0

prime :: Integer -> Bool
prime n = n > 1 && and [not(divides x n) | x <- [2..(n-1)]]

primePair :: Integer -> (Integer, Bool)
primePair n = (n, prime n)

allprimes :: [Integer]
allprimes = [x | x<- [2..], prime x]

allprimesBetween :: Integer -> Integer -> [Integer]
allprimesBetween x y = filter (\x -> prime x) [x..y]

primeTest :: [Bool]
primeTest = map (prime) [1..]

pairTest :: [(Integer, Bool)]
pairTest = map (primePair) [1..]

isPrimeTwin :: Integer -> Bool
isPrimeTwin n = prime n && prime (n - 2)

allPrimeTwins :: [Integer]
allPrimeTwins = filter (isPrimeTwin) [0..]

-- *Main> primeTwins 2000
-- 302
primeTwins n = do
    let pt = take n allprimes
    let distribution = map (isPrimeTwin) pt
    length (filter (\x -> x == True) distribution)