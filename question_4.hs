expmod :: Int -> Int -> Int -> Int
expmod m 0 n = 1 `mod` n
expmod m e n = ((expmod m (e-1) n) * m) `mod` n

expmodfast :: Int -> Int -> Int -> Int
expmodfast m e n 
    | e == 0         =  1 `mod` n
    | e `mod` 2 == 0 =  ((expmodfast m (e `div` 2) n)^2) `mod` n
    | otherwise      =  ((expmodfast m (e-1) n) * m) `mod` n

-- expmodfast 125 963560 126 = 1
-- expmodfast 130 965677 140 = 60
-- expmodfast 120 963810 130 = 40