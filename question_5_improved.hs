import Data.List
import Debug.Trace

(!!!)                :: [[Bool]] -> Maybe Int -> [Bool]
xs     !!! Nothing        = []
[]     !!! _              = []
(x:_)  !!! Just 0         = x
(_:xs) !!! Just n         = xs !!! Just (n-1)

(!!!!)               :: [Bool] -> Int -> Bool
xs     !!!! n | n < 0 =  False
[]     !!!! _         =  False
(x:_)  !!!! 0         =  x
(_:xs) !!!! n         =  xs !!!! (n-1)

maybeSub :: Maybe Int -> Int -> Maybe Int
maybeSub Nothing x = Just (-1)
maybeSub (Just y) x =  Just (y - x)

maybeAdd :: Maybe Int -> Int -> Maybe Int
maybeAdd Nothing x = Just (-1)
maybeAdd (Just y) x = Just (y + x)

valid :: [[Bool]] -> Bool
valid xs = validaux xs (length xs) && (length xs >= 3)
  where
    validaux [] y = True
    validaux (x:xs) y = (length x) == y && validaux xs y 


showGrid :: [[Bool]] -> String
showGrid [] = ""
showGrid (x:xs) = showLine x ++ showGrid xs
  where
    showLine [] = "\n"
    showLine (x:xs)  | x         = "0" ++ showLine xs
                     | otherwise = "." ++ showLine xs

printGrid :: [[Bool]] -> IO ()
printGrid x = putStrLn (showGrid x)


readGrid :: String -> [[Bool]]
readGrid x = readGridAux (lines x)
  where
    readGridAux [] = []
    readGridAux (x:xs) = [readLine x] ++ readGridAux xs
      where
        readLine [] = []
        readLine (x:xs) | x == '0' = True : readLine xs
                        | x == '.' = False : readLine xs


neighbours :: [Bool] -> Int
neighbours [] = 0
neighbours (x:xs) | x == True = 1 + neighbours xs
                  | otherwise = neighbours xs

newCell :: Int -> Bool -> Bool
--newCell x y | y == True  && (x < 2 || x > 3) = False
  --          | y == True && (x == 2 || x == 3) = True
    --        | y == False && x == 3  = True
      --      | otherwise             = False
newCell x y | x == 2 = y
            | x == 3 = True
            | otherwise = False

step :: [[Bool]] -> [[Bool]]
step (x:xs) = if (valid (x:xs)) 
                then processGrid (x:xs) 0 neighbourList
              else 
                error "Invalid grid"
              where
                grid = (x:xs)
                neighbourList = [
                   trace ("Fst: " ++ (show (fst (splitAt colIndex row))) ++ ", snd: " ++ (show (snd (splitAt colIndex row)))) $
                  ((reverse (fst (splitAt colIndex (grid !!! ((Just rowIndex) `maybeSub` 1))))) !!!! 1) : ((grid !!! ((Just rowIndex) `maybeSub` 1)) !!!! colIndex) : ((snd (splitAt colIndex (grid !!! ((Just rowIndex) `maybeSub` 1)))) !!!! 0)
                  :
                  ((reverse (fst (splitAt colIndex row))) !!!! 1) : ((snd (splitAt colIndex row)) !!!! 0)
                  :
                  ((reverse (fst (splitAt colIndex (grid !!! ((Just rowIndex) `maybeAdd` 1))))) !!!! 1) : ((grid !!! ((Just rowIndex) `maybeAdd` 1)) !!!! colIndex) : ((snd (splitAt colIndex (grid !!! ((Just rowIndex) `maybeAdd` 1)))) !!!! 0)
                  :
                  []
                  |
                  (rowIndex, row) <- zip [0..] grid, (colIndex, col) <- zip [0..] row]

processGrid :: [[Bool]] -> Int -> [[Bool]] -> [[Bool]]
processGrid [] row nList = []
processGrid (x:xs) row nList = trace ("\nProcessing row no " ++ (show row)) $ [processLine x (x:xs) row 0 nList ] ++ processGrid xs (row + 1) nList

processLine :: [Bool] -> [[Bool]] -> Int -> Int -> [[Bool]] -> [Bool]
processLine [] grid row col nList = []
processLine (x:xs) grid row col nList = trace ("\nProcessing col no " ++ (show col)) $ newCell (neighbours (trace ("Neighbours: " ++ show(nList !! (row+col))) $ nList !! (row+col))) x : processLine xs grid row (col + 1) nList
  


--where
--    curLine = (x:xs)
  --  neighboursList = gs ++ hs ++ js
    --  where
      --  hs = ((reverse (fst (splitAt col curLine))) !!!! 1) : ((snd (splitAt col curLine)) !!!! 0) : []
        -- gs = ((grid !!! ((Just row) `maybeSub` 1)) !!!! col) : ((reverse (fst (splitAt col (grid !!! ((Just row) `maybeSub` 1))))) !!!! 1) : ((snd (splitAt col (grid !!! ((Just row) `maybeSub` 1)))) !!!! 0) : []
        -- js = ((grid !!! ((Just row) `maybeAdd` 1)) !!!! col) : ((reverse (fst (splitAt col (grid !!! ((Just row) `maybeAdd` 1))))) !!!! 1) : ((snd (splitAt col (grid !!! ((Just row) `maybeAdd` 1)))) !!!! 0) : []






 