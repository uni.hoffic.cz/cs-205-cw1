-- Group 80
-- 963810 - Petr Hoffmann
-- 963560 - Krzysztof Bielikowicz
-- 965677 - Mateusz Meller

import Test.QuickCheck
import Data.List
import Data.Char
import Data.List.Split
import System.Random

----------------------
-- QUESTION 1 START --
----------------------

average :: Float -> Float -> Float -> Float
average x y z = (x + y + z) / 3

-- *Main> howManyAboveAverage1 963810 965677 963560
-- 1
howManyAboveAverage1 :: Float -> Float -> Float -> Integer
howManyAboveAverage1 x y z = do
    let cAvg = average x y z
    scabav x cAvg + scabav y cAvg + scabav z cAvg

scabav :: Float -> Float -> Integer
scabav n average =
    if n > average then
        1
    else
        0

-- *Main> howManyAboveAverage2 963810 965677 963560
-- 1
howManyAboveAverage2 :: Float -> Float -> Float -> Integer
howManyAboveAverage2 x y z = greaterCounterToInt (greaterCounterRec ([x, y, z], 0, (average x y z)))

greaterCounterToInt :: ([Float], Integer, Float) -> Integer
greaterCounterToInt (xs, n, avg) = n

greaterCounterRec :: ([Float], Integer, Float) -> ([Float], Integer, Float)
greaterCounterRec ([], n, avg) = ([], n, avg)
greaterCounterRec (x:xs, n, avg) =
    if x > avg then
        greaterCounterRec (xs, n + 1, avg)
    else
        greaterCounterRec (xs, n, avg)

checkcorrectness :: Float -> Float -> Float -> Bool
checkcorrectness x y z =
    howManyAboveAverage1 x y z == howManyAboveAverage2 x y z

-- *Main> quickCheck checkcorrectness
-- +++ OK, passed 100 tests.

--------------------
-- QUESTION 1 END --
--------------------



----------------------
-- QUESTION 2 START --
----------------------

-- GBP / cm^2
pizza_base_cost :: Float
pizza_base_cost = 0.001

-- GBP / cm^2
pizza_topping_cost :: Float
pizza_topping_cost = 0.002

-- 1.0 means no margin, 1.5 means 0.5 is the margin
price_margin_multiplier :: Float
price_margin_multiplier = 1.5

alfredo :: Float -> Float -> Float
alfredo diameter numberOfToppings = do
    let pizzaSurface = pi * (diameter / 2) ** 2
    let pizzaBaseCost = pizzaSurface * pizza_base_cost
    let pizzaToppingsCost = pizzaSurface * pizza_topping_cost * numberOfToppings
    let pizzaCost = pizzaBaseCost + pizzaToppingsCost
    let pizzaPrice = pizzaCost * price_margin_multiplier
    pizzaPrice

-- The Bambini pizza is NOT more expensive than the Famiglia one. See below:
-- *Main> bambiniMoreExFamiglia
-- False
bambiniMoreExFamiglia :: Bool
bambiniMoreExFamiglia = alfredo 16 6 > alfredo 32 2

--------------------
-- QUESTION 2 END --
--------------------



----------------------
-- QUESTION 3 START --
----------------------

divides :: Integer -> Integer -> Bool
divides x y = y `mod` x == 0

prime :: Integer -> Bool
prime n = n > 1 && and [not(divides x n) | x <- [2..(n-1)]]

primePair :: Integer -> (Integer, Bool)
primePair n = (n, prime n)

allprimes :: [Integer]
allprimes = [x | x<- [2..], prime x]

allprimesBetween :: Integer -> Integer -> [Integer]
allprimesBetween x y = filter (\x -> prime x) [x..y]

primeTest :: [Bool]
primeTest = map (prime) [1..]

pairTest :: [(Integer, Bool)]
pairTest = map (primePair) [1..]

isPrimeTwin :: Integer -> Bool
isPrimeTwin n = prime n && prime (n - 2)

allPrimeTwins :: [Integer]
allPrimeTwins = filter (isPrimeTwin) [0..]

-- *Main> primeTwins 2000
-- 302
primeTwins n = do
    let pt = take n allprimes
    let distribution = map (isPrimeTwin) pt
    length (filter (\x -> x == True) distribution)

--------------------
-- QUESTION 3 END --
--------------------



----------------------
-- QUESTION 4 START --
----------------------

expmod :: Int -> Int -> Int -> Int
expmod m 0 n = 1 `mod` n
expmod m e n = ((expmod m (e-1) n) * m) `mod` n

expmodfast :: Int -> Int -> Int -> Int
expmodfast m e n
    | e == 0         =  1 `mod` n
    | e `mod` 2 == 0 =  ((expmodfast m (e `div` 2) n)^2) `mod` n
    | otherwise      =  ((expmodfast m (e-1) n) * m) `mod` n

-- expmodfast 125 963560 126 = 1
-- expmodfast 130 965677 140 = 60
-- expmodfast 120 963810 130 = 40

--------------------
-- QUESTION 4 END --
--------------------



----------------------
-- QUESTION 5 START --
----------------------

-- a special implementation of the !! operator, which only operates on [[Bool]] lists and
-- returns an empty list if the index is out of bounds or the list is empty
(!!!)                :: [[Bool]] -> Int -> [Bool]
xs     !!! n | n < 0      = []
[]     !!! _              = []
(x:_)  !!! 0              = x
(_:xs) !!!  n             = xs !!! (n-1)

-- a special implementation of the !! operator, which only operates on [Bool] lists and
-- returns false if the index is out of bounds or the list is empty
(!!!!)               :: [Bool] -> Int -> Bool
xs     !!!! n | n < 0 =  False
[]     !!!! _         =  False
(x:_)  !!!! 0         =  x
(_:xs) !!!! n         =  xs !!!! (n-1)

-- checks if the grid is valid (is a rectangle of minimum 3x3 size)
valid :: [[Bool]] -> Bool
valid xs = validaux xs (length (xs !!! 0)) && (length xs >= 3)
  where
    validaux [] y = True
    validaux (x:xs) y = (length x) == y && validaux xs y

-- computes the number of rows in the grid
getNumRows :: [[Bool]] -> Int
getNumRows grid = length grid

-- computes the number of columns in the grid
getNumCols :: [[Bool]] -> Int
getNumCols grid = length (grid !!! 0)

--generates a random grid of a given size
generateGrid :: Int -> IO [[Bool]]
generateGrid x = do g <- newStdGen
                    let randomNumbers = randomRs (0 :: Int,1 :: Int) g
                    -- for all elements of the grid, we randomly choose '.','*' or, if we are at the end of the line, a newline character '\n'
                    return (readGrid ([if (y`mod`(x+1) == 0) then '\n' else (['.','*'] !! (randomNumbers !! y))| y <- [0..((x*x)+x)], y /= 0]))

-- transforms a [[Bool]] grid to its string representation
showGrid :: [[Bool]] -> String
showGrid [] = ""
showGrid (x:xs) = showLine x ++ showGrid xs
  where
    showLine [] = "\n"
    showLine (x:xs)  | x         = "*" ++ showLine xs
                     | otherwise = "." ++ showLine xs

-- prints a [[Bool]] grid to the console
printGrid :: [[Bool]] -> IO ()
printGrid x = putStrLn (showGrid x)

-- transforms a grid from its string representation to [[Bool]]
readGrid :: String -> [[Bool]]
readGrid x = readGridAux (lines x)
  where
    readGridAux [] = []
    readGridAux (x:xs) = [readLine x] ++ readGridAux xs
      where
        readLine [] = []
        readLine (x:xs) | x == '*' = True : readLine xs
                        | x == '.' = False : readLine xs


-- returns the number of alive neighbours in a given list of neighbours
neighbours :: [Bool] -> Int
neighbours [] = 0
neighbours (x:xs) | x == True = 1 + neighbours xs
                  | otherwise = neighbours xs

-- decides the fate of a cell - changes its state depending on the number of alive neighbours
newCell :: Int -> Bool -> Bool
newCell x y | x == 2 = y
            | x == 3 = True
            | otherwise = False

-- computes the list of neighbours of a cell in the given position in the given grid
getNeighbours :: (Int,Int) -> [[Bool]] -> [Bool]
-- for all 'coordinate' pairs (a,b), where a belongs to (x-1..x+1) and b belongs to (y-1..y+), except for when either a=x or b=y,
-- we add cells which are represented by those 'coordinates' to the output list
getNeighbours (x,y) grid = map (\z -> ((grid !!! (fst z)) !!!! snd z)) [(a,b) | a <- [(x-1)..(x+1)], b <- [(y-1)..(y+1)], (x /= a || y /= b)]

-- updates a given grid to the next step
step :: [[Bool]] -> [[Bool]]
step (x:xs) = if (valid (x:xs))
                then processGrid (x:xs)
              else
                error "Invalid grid"

-- a helper function that performs the update on a given grid
processGrid :: [[Bool]] -> [[Bool]]
-- for all pairs of 'coordinates' in the grid, we perform the newCell operation given its neighbours
-- and the cell itself by mapping the operation to the list of pairs; we then use chunksOf (dim) [] to
-- split the computed list into [[Bool]] grid
processGrid xss = chunksOf (numCols) (map (\z -> newCell (neighbours (getNeighbours (fst z, snd z) xss)) ((xss !!! (fst z)) !!!! snd z)) [(a,b) | a <- [0..(numRows -1)], b <- [0..(numCols -1)]])
  where
    numRows = getNumRows xss
    numCols = getNumCols xss

-- loads a file from the given path and returns it as a [[Bool]]
loadGridFile :: String -> IO [[Bool]]
loadGridFile filePath = do content <- (readFile filePath)
                           let grid =  (readGrid content)
                           if valid grid
                             then (return grid)
                           else
                             error "Invalid grid"

-- loads an encoded file from the given path and returns it as a [[Bool]]
loadEncodedGridFile :: String -> IO [[Bool]]
loadEncodedGridFile filePath = do encodedContent <- (readFile filePath)
                                  let grid = (readGrid (decodeGrid encodedContent))
                                  return grid

-- encodes a string representation of a grid using run-length encoding
encodeGrid :: String -> String
encodeGrid xs = encodeAux (group xs)
  where
    encodeAux [] = []
    encodeAux (x:xs) | length x == 1 = head(x) : encodeAux xs
                     | otherwise     = show(length x) ++ (head(x) : (encodeAux xs))

-- decodes a run-length encoded string representation of a grid
decodeGrid :: String -> String
decodeGrid []     = []
-- if the current character is a digit, take all subsequent integers from the list, merge them into a number,
-- read the letter following the number and repeat in the number of times
decodeGrid (x:xs) | isDigit x = [nextLetter | y <- [1..(read (takeWhile (isDigit) (x:xs)))]] ++ decodeGrid (tail (dropWhile (isDigit) (x:xs)))
                  | otherwise = x : decodeGrid xs
                    where nextLetter = head(dropWhile (isDigit) (x:xs))

-- basic version of the interactive program, which asks user for a file name and then performs steps while the user presses enter
interactive :: IO()
interactive = do putStrLn "Enter a file name: "
                 fileName <- getLine
                 grid <- loadGridFile (fileName)
                 printGrid(grid)
                 simulation grid
                   where
                     simulation :: [[Bool]] -> IO()
                     simulation grid = do userInput <- getLine
                                          if userInput == ""
                                            then do let temp = step(grid)
                                                    printGrid(temp)
                                                    simulation temp
                                          else if userInput == "exit"
                                            then return ()
                                          else
                                            simulation grid

-- an IMPROVED version of the interactive program:
-- 1) allows the user to choose if they want to read in a grid from a file or generate a random one
-- 2) allows the user to save current state of the grid into a file
-- 3) allows the user to choose if they want to load or save the grid in either standard or encoded format
interactiveImproved :: IO()
interactiveImproved = do putStrLn "Enter 'random' to generate a random grid or 'file' to load it from a file: "
                         userChoice <- getLine
                         if userChoice == "random"
                           then do putStrLn "Enter the size of the randomly generated grid: "
                                   size <- getLine
                                   grid <- generateGrid (read size)
                                   printGrid(grid)
                                   simulation grid
                         else if userChoice == "file"
                           then do putStrLn "Enter 'encoded' to read an encoded file or 'standard' to read a standard file: "
                                   userInput <- getLine
                                   if userInput == "encoded"
                                     then do putStrLn "Enter a file name: "
                                             fileName <- getLine
                                             grid <- loadEncodedGridFile (fileName)
                                             printGrid(grid)
                                             simulation grid
                                   else if userInput == "standard"
                                     then do putStrLn "Enter a file name: "
                                             fileName <- getLine
                                             grid <- loadGridFile (fileName)
                                             printGrid(grid)
                                             simulation grid
                                   else
                                     interactiveImproved
                         else
                           interactiveImproved

                           where
                             simulation :: [[Bool]] -> IO()
                             simulation grid = do userInput <- getLine
                                                  if userInput == ""
                                                    then do let temp = step(grid)
                                                            printGrid(temp)
                                                            simulation temp
                                                  else if userInput == "exit"
                                                    then return ()
                                                  else if userInput == "save"
                                                    then do putStrLn "Enter 'encoded' to save the grid in an encoded format or 'standard' to save it in a standard format: "
                                                            userChoice <- getLine
                                                            if userChoice == "encoded"
                                                              then do putStrLn "Enter a file name to save the current grid to: "
                                                                      fileName <- getLine
                                                                      writeFile fileName (encodeGrid(showGrid(grid)))
                                                                      simulation grid
                                                            else if userChoice == "standard"
                                                              then do putStrLn "Enter a file name to save the current grid to: "
                                                                      fileName <- getLine
                                                                      writeFile fileName (showGrid(grid))
                                                                      simulation grid
                                                            else
                                                              simulation grid
                                                  else
                                                    simulation grid


--------------------
-- QUESTION 5 END --
--------------------