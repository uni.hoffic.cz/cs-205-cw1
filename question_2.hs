-- GBP / cm^2
pizza_base_cost :: Float
pizza_base_cost = 0.001

-- GBP / cm^2
pizza_topping_cost :: Float
pizza_topping_cost = 0.002

-- 1.0 means no margin, 1.5 means 0.5 is the margin
price_margin_multiplier :: Float
price_margin_multiplier = 1.5

alfredo :: Float -> Float -> Float
alfredo diameter numberOfToppings = do
    let pizzaSurface = pi * (diameter / 2) ** 2
    let pizzaBaseCost = pizzaSurface * pizza_base_cost
    let pizzaToppingsCost = pizzaSurface * pizza_topping_cost * numberOfToppings
    let pizzaCost = pizzaBaseCost + pizzaToppingsCost
    let pizzaPrice = pizzaCost * price_margin_multiplier
    pizzaPrice

-- The Bambini pizza is NOT more expensive than the Famiglia one. See below:
-- *Main> bambiniMoreExFamiglia
-- False
bambiniMoreExFamiglia :: Bool
bambiniMoreExFamiglia = alfredo 16 6 > alfredo 32 2