import Test.QuickCheck

average :: Float -> Float -> Float -> Float
average x y z = (x + y + z) / 3

-- *Main> howManyAboveAverage1 963810 965677 963560
-- 1
howManyAboveAverage1 :: Float -> Float -> Float -> Integer
howManyAboveAverage1 x y z = do
    let cAvg = average x y z
    scabav x cAvg + scabav y cAvg + scabav z cAvg

scabav :: Float -> Float -> Integer
scabav n average =
    if n > average then
        1
    else
        0

-- *Main> howManyAboveAverage2 963810 965677 963560
-- 1
howManyAboveAverage2 :: Float -> Float -> Float -> Integer
howManyAboveAverage2 x y z = greaterCounterToInt (greaterCounterRec ([x, y, z], 0, (average x y z)))

greaterCounterToInt :: ([Float], Integer, Float) -> Integer
greaterCounterToInt (xs, n, avg) = n

greaterCounterRec :: ([Float], Integer, Float) -> ([Float], Integer, Float)
greaterCounterRec ([], n, avg) = ([], n, avg)
greaterCounterRec (x:xs, n, avg) =
    if x > avg then
        greaterCounterRec (xs, n + 1, avg)
    else
        greaterCounterRec (xs, n, avg)

checkcorrectness :: Float -> Float -> Float -> Bool
checkcorrectness x y z =
    howManyAboveAverage1 x y z == howManyAboveAverage2 x y z

-- *Main> quickCheck checkcorrectness
-- +++ OK, passed 100 tests.